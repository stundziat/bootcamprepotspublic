package com.mongodb.mongodb.repository;

import com.mongodb.mongodb.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "employee", path = "employee")
public interface EmployeeRepo extends MongoRepository<Employee, String>
{

}
