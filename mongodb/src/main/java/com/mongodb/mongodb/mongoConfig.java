package com.mongodb.mongodb;

// Spring config file (mongo config)

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.mongodb.mongodb.repository")
public class mongoConfig extends AbstractMongoConfiguration
{
    @Override
    protected String getDatabaseName()
    {
        return "test";
    }

    @Override
    public MongoClient mongoClient()
    {
        return new MongoClient("127.0.0.1", 27017);
    }
}
