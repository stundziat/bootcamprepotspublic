package com.madhatter.firstserver.controller;


import com.madhatter.firstserver.model.StoreItem;
import com.madhatter.firstserver.repository.StoreItemRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class StoreItems
{
    private final StoreItemRepository itemRepo;

    public StoreItems(StoreItemRepository itemRepo)
    {
        this.itemRepo = itemRepo;
    }

    @GetMapping("/items")
    public List<StoreItem> getItems()
    {
        return (List<StoreItem>) itemRepo.findAll();
    }
}
