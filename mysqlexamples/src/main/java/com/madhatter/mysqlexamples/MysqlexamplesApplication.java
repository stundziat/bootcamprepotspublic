package com.madhatter.mysqlexamples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlexamplesApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(MysqlexamplesApplication.class, args);
    }

}
